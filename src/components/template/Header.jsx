import './Header.css'
import React from 'react'

export default props =>
    <header className="header d-none d-sm-flex flex-column">
        <h1 className="mt-3">
            <i className={`fa fa-${props.icon}`}></i> {props.title} 
        </h1>
        
        <aside className="menus-area">   
        <nav className="menus">
        <a href="#/login">
            <i className="fa fa-register"></i>CADASTRAR
        </a>

        </nav>
    </aside>
    </header>