/*import React from 'react'
import NavItem from '../template/navItem'
import { AuthConsumer } from '../../main/provedorAutenticacao'
import logo from '../../assets/images/fourcicle.png'

    function Nav(props){
    return (
        <div className="navbar navbar-expand-lg fixed-top navbar-dark bg-primary">

        <div className="container">
        
          <a href="#/home" className="menu-area" ></a>
          <div className="menu">
            <img src={logo} alt="logo" />
          </div>
          <button className="navbar-toggler" type="button" 
                  data-toggle="collapse" data-target="#navbarResponsive" 
                  aria-controls="navbarResponsive" aria-expanded="false" 
                  aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarResponsive">
            <ul className="navbar-nav">
                
                <NavItem render={props.isUsuarioAutenticado} href="#/home" label="Home" />
                <NavItem render={props.isUsuarioAutenticado} onClick={props.deslogar} href="#/login" label="Sair" />
            </ul>
            </div>
        </div>
      </div>
    )
}

export default () => (
  <AuthConsumer>
    {(context) => (
        <Nav isUsuarioAutenticado={context.isAutenticado} deslogar={context.encerrarSessao} />
    )}
  </AuthConsumer>
)*/


import React from 'react'
import NavItem from '../template/navItem'
import { AuthConsumer } from '../../main/provedorAutenticacao'
import logo from '../../assets/images/fourcicle.png'
import './Nav.css'
function Nav(props){
    return (
        <div className="navbar navbar-expand-lg fixed-top navbar-dark bg-primary">
        <div className="container">
          <a href="#/home" className="Fourcicle"></a>
          <button className="navbar-toggler" type="button" 
                  data-toggle="collapse" data-target="#navbarResponsive" 
                  aria-controls="navbarResponsive" aria-expanded="false" 
                  aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarResponsive">
            <ul className="navbar-nav">
                <NavItem render={props.isUsuarioAutenticado} href="#/home" label="Home" />
                <NavItem render={props.isUsuarioAutenticado} onClick={props.deslogar} href="#/login" label="Sair" />
            </ul>
            </div>
        </div>
      </div>
    )
}

export default () => (
  <AuthConsumer>
    {(context) => (
        <Nav isUsuarioAutenticado={context.isAutenticado} deslogar={context.encerrarSessao} />
    )}
  </AuthConsumer>
)