
//import Solicitacoes from '../views/Solicitacoes'
//import novaSolicitacao from '../views/novaSolicitacao'
//import solicitacaoSucesso from '../views/solicitacaoSucesso'

/*export default props =>
    <Switch>
        <Route path="/index" component={Home} />
        <Route path='/cadastro' component={UsuarioCadastro} />
        <Route path='/login' component={Login} />
        <Route path='/solicitacao' component={Solicitacoes} />
        <Route path='/novaSolicitacao' component={novaSolicitacao} />
        <Route path='/solicitacaoSucesso' component={solicitacaoSucesso} />
        <Redirect from='*' to='/index' />
    </Switch>*/


    import React from 'react'

    import Home from '../views/Home'
    import UsuarioCadastro from '../views/UsuarioCadastro'
    import Login from '../views/Login'
    import Solicitacoes from '../views/solicitacoes'
    import novaSolicitacao from '../views/novaSolicitacao'
    import solicitacaoSucesso from '../views/solicitacaoSucesso'

    import { AuthConsumer } from './provedorAutenticacao'
    import { Route, Switch, HashRouter, Redirect } from 'react-router-dom'
    function RotaAutenticada( { component: Component, isUsuarioAutenticado, ...props } ){
        return (
            <Route {...props} render={ (componentProps) => {
                if(isUsuarioAutenticado){
                    return (
                        <Component {...componentProps} />
                    )
                }else{
                    return(
                        <Redirect to={ {pathname : '/login', state : { from: componentProps.location } } } />
                    )
                }
            }}  />
        )
    }
    
    function Routes(props){
        return (
            <HashRouter>
                <Switch>
                    <Route path='/cadastro' component={UsuarioCadastro} />
                    <Route path='/login' component={Login} />
                    <Route path='/solicitacoes' component={Solicitacoes} />
                    <Route path='/novaSolicitacao' component={novaSolicitacao} />
                    <Route path='/solicitacaoSucesso' component={solicitacaoSucesso} />
                    <RotaAutenticada isUsuarioAutenticado={props.isUsuarioAutenticado} path="/home" component={Home} />
                </Switch>
            </HashRouter>
        )
    }
    
    export default () => (
        <AuthConsumer>
            { (context) => (<Routes isUsuarioAutenticado={context.isAutenticado} />) }
        </AuthConsumer>
    )