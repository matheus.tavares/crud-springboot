import 'bootstrap/dist/css/bootstrap.min.css'
import 'font-awesome/css/font-awesome.min.css'
import React from 'react'

import ProvedorAutenticacao from './provedorAutenticacao'

import 'toastr/build/toastr.min.js'

import Routes from '../../src/main/Routes'
import Nav from '../components/template/Nav'
import '../components/usuario/custom.css'
import 'toastr/build/toastr.css'
import './App.css'

class App extends React.Component {

render(){
    return(
        <ProvedorAutenticacao>
        <Nav />
        <div className="container">  
            <Routes />
        </div>
      </ProvedorAutenticacao>
    )
  }
}
export default App

/*<BrowserRouter>
    <div className="app">
        <Logo />
        <Nav />
        <Routes />
        <Footer />
    </div>
</BrowserRouter>

export default App*/
