import React from 'react'
import './Home.css'
import UsuarioService from '../app/service/usuarioService'
import { AuthContext } from '../main/provedorAutenticacao'


class Home extends React.Component{

    constructor(){
        super()
        this.usuarioService = new UsuarioService();
    }

    componentDidMount(){
        const usuarioLogado = this.context.usuarioAutenticado
    }

    /*render(){
        return (
                <div className="fundo">

                    <h1 className="inition">Portal da Lei Geral de <br/> Proteção de Dados Pessoais                  
                    
                    <Link to="/login">
                    <a className="btn "
                    role="botaodois"><i className="f"></i> Acessar o sistema
                    </a>
                    </Link>
                    
                    </h1>
                    <p className="lead">
                    </p>
                </div>
        )
    }*/

    render(){
        return (
            <div className="fundo">
                
                <h1 className="inition">Portal da Lei Geral de <br/> Proteção de Dados Pessoais</h1>
                <p className="lead">
                    <a className="btn btn-primary btn-lg" 
                    href="#/login" 
                    role="button"><i className="pi pi-users"></i>  
                     Acessar sistema
                    </a>
                </p>
            </div>
        )
    }
}

Home.contextType = AuthContext;

export default Home