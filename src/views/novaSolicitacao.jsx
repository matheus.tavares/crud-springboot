import React from 'react'
import Card from '../components/card'
import './novaSolicitacao.css'
import FormGroup from '../components/form-group'
import SelectMenu from '../components/selectMenu'
import { Link } from 'react-router-dom'

class novaSolicitacao extends React.Component{

    constructor(props){
        super(props)
        this.state ={list: false}
    }


render(){ 
   const mudancaState =() =>{
            this.setState({list: !this.state.list})
        }
        const lista = [
            { label: 'Selecione as informações', value: '' },
            { label: 'Lorem ipsum dolor', value: '1' },
            { label: 'Sit amet, consectetur', value: '2' },
            { label: 'Adsipling elite', value: '3' },
            { label: 'kim jon un', value: '4' },
            { label: 'um dos tre', value: '5' },

        ]

        
        return(
            <div className="containernovasolicitacao">
                <h2>Nova Solicitação</h2>
                    <Card title= "Solicitações / Nova Solicitação"></Card>
                            <FormGroup>
                                <div className="hidi">
                                    <b>1) Escolha um tipo de serviço</b>
                                </div>
                                    <div className="acesso1">
                                        <button onClick={mudancaState} className="botaonovabr">
                                            <h5 className="acessooo">Acesso</h5>
                                            o acesso é usado para fazer uma solicitação de todos os <br/>
                                            dados que a empresa
                                        </button>
                                    </div>
                            </FormGroup>
                                
                            <FormGroup>
                            <div className="headertopopag">
                                </div>
                                    <div className="acesso2">
                                        <button className="botaonovabr">
                                            <h5 className="acessooo">Correção</h5>
                                            A correção é usada para quando existe algum problema <br/>
                                            com seus dados
                                        </button>
                                    </div>
                            </FormGroup>

                            <FormGroup>
                            <div className="headertopopag">
                                </div>
                                    <div className="acesso3">
                                        <button className="botaonovabr">
                                            <h5 className="acessooo">Anonimização</h5>
                                            A correção é usada para quando existe algum problema <br/>
                                            com seus dados
                                        </button>
                                    </div>
                            </FormGroup>

                            <FormGroup>
                            <div className="headertopopag">
                                </div>
                                    <div className="acesso4">
                                        <button className="botaonovabr">
                                            <h5 className="acessooo">Portabilidade</h5>
                                            A correção é usada para quando existe algum problema <br/>
                                            com seus dados
                                        </button>
                                    </div>
                            </FormGroup>

                            <FormGroup>
                            <div className="headertopo2">
                                </div>
                                    <div className="acesso5">
                                        <button className="botaonovabr">
                                            <h5 className="acessooo">Eliminação</h5>
                                            A correção é usada para quando existe algum problema <br/>
                                            com seus dados
                                        </button>
                                    </div>
                            </FormGroup>

                            <FormGroup>
                            <div className="headertopopag">
                                </div>
                                    <div className="acesso6">
                                        <button className="botaonovabr">
                                            <h5 className="acessooo">Revogamento de Consentimento</h5>
                                            A correção é usada para quando existe algum problema <br/>
                                            com seus dados
                                        </button>
                                    </div>
                            </FormGroup>
                            
                            <div className="btngo">
                                <Link to="/solicitacaoSucesso">
                                    <a className="botaumm "
                                        role="botaodois"><i className="botaumm"></i> solicitar
                                    </a>
                                </Link>
                                </div>

                            {this.state.list && <FormGroup htmlFor="inputDados" input="informacoes" className="formulariodoisz">
                                <div className="information">
                                    <div className="godois">
                                        <b>2) Qual a informação que você deseja receber</b>
                                    </div>
                                    <br/>
                                        <div className="gotres">
                                            <h6 className="">Informação *</h6> <br/>
                                        </div>
                                        <div className="goquatro">
                                            <SelectMenu lista={lista} />
                                        </div>
                                        <br/>
                                        <br/>
                                        <div className="gocinco">
                                            <h6>Descreve o motivo de sua solicitação</h6>
                                        </div>
                                        
                                        <div class="field" className="goceis">
                                            <textarea name="mensagem" id="mensagem" placeholder="Mensagem*" required></textarea>
                                        </div>
                                    <br/>
                                    <br/>
                                    <div className="gocete">
                                        <h6>Anexe imagens ou documentos que você deseja enviar e que <br/>
                                        facilitam ou tornam explicitos os motivos da sua requisição
                                    </h6>
                                    </div>
                                    <br/>
                                    <div className="gooito">
                                        <input type="hidden" name="MAX_FILE_SIZE" value="4194304" />
                                        <input type="file" />
                                    </div>
                                    
                                </div>        

                                <br/>
                            </FormGroup>}    
        </div>
        )
    }

}

export default novaSolicitacao