import React from 'react'
import './solicitacoes.css'
import { Link } from 'react-router-dom'

class Solicitacoes extends React.Component{

    render(){
        return (
            <div className="initSolicitacao">
                <h3 className="iniciando">Solicitações</h3>
                <p className="lead"></p>
                <Link to="/novaSolicitacao">
                    <button className="btnprim" type="btn btn-primary">+ <br/> Nova <br/> Solicitação</button>
                </Link>
            </div>
        )
    }
}

export default Solicitacoes