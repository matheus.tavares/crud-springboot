/*import React from 'react'

import Card from '../components/usuario/card'
import UsuarioService from '../app/service/usuarioService'
import { mensagemSucesso, mensagemError } from '../components/toastr'
import FormGroup from '../components/usuario/form-group'
import { withRouter} from 'react-router'
import { Link } from 'react-router-dom'

class UsuarioCadastro extends React.Component{

    state = {
        nome : '',
        email: '',
        senha: '',
        senhaRepeticao : ''
    }

    constructor(){
        super();
        this.service = new UsuarioService();
    }

    validar(){
        const msgs = []

        if(!this.state.nome){
            msgs.push('O campo Nome é obrigatório')
        }

        if(!this.state.email){
            msgs.push('O campo Eail é obrigatório')
        }else if( !this.state.email.match(/^[a-z0-9.]+@[a-z0-9]+\.[a-z]/) ){
            msgs.push('Informe um Email Válido')
        }

        if(!this.state.senha || !this.state.senhaRepeticao){
            msgs.push('Digite a senha duas vezes')
        }else if(this.state.senha !== this.state.senhaRepeticao){
            msgs.push('As Senhas não São iguais, Por favor digite novamente')
        }

        return msgs;
    }

    cadastrar = () => {
    const msgs = this.validar();

    if(msgs && msgs.length > 0){
        msgs.forEach( (msg, index ) => {
            mensagemError(msg)
        });
        return false;
    }

        const usuario = {
            nome: this.state.nome,
            email: this.state.email,
            senha: this.state.senha
        }

        this.service.salvar(usuario)
            .then( response => {
                mensagemSucesso('Usuario cadastrado com sucesso!')
                this.props.history.push('/login')
            }).catch(error => {
                mensagemError(error.response.data)
            })
    }

    render(){
        return (
            <Card title="Cadastro de Usuario">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="bs-component">
                            <FormGroup label="nome: " htmlFor="inputNome">
                                <input type="text"
                                 id="inputNome"
                                 className="form-control"
                                 name="nome"
                                 onChange={e => this.setState({nome: e.target.value})} />
                            </FormGroup>

                            <FormGroup label="Email: " htmlFor="inputEmail">
                                <input type="email"
                                 id="inputEmail"
                                 className="form-control"
                                 name="email"
                                 onChange={e => this.setState({email: e.target.value})} />
                            </FormGroup>

                            <FormGroup label="Senha: " htmlFor="inputSenha">
                                <input type="password"
                                 id="inputSenha"
                                 className="form-control"
                                 name="senha"
                                 onChange={e => this.setState({senha: e.target.value})} />
                            </FormGroup>

                            <FormGroup label="Repita a Senha: " htmlFor="inputRepitaSenha">
                                <input type="password"
                                 id="inputRepitaSenha"
                                 className="form-control"
                                 name="senha"
                                 onChange={e => this.setState({nome: e.target.value})} />
                            </FormGroup>
                                <button onClick={this.cadastrar} type="button" className="btn btn-sucess">Salvar</button>
                            
                            
                        </div>
                    </div>
                </div>
            </Card>
        )
    }
}

export default withRouter(UsuarioCadastro)

*/


import React from 'react'

import { withRouter } from 'react-router-dom'
import Card from '../components/card'
import FormGroup from '../components/form-group'

import UsuarioService from '../app/service/usuarioService'
import { mensagemSucesso, mensagemError } from '../components/toastr'

class UsuarioCadastro extends React.Component{

    state = {
        nome : '',
        email: '', 
        senha: '',
        senhaRepeticao : ''
    }

    constructor(){
        super();
        this.service = new UsuarioService();
    }

    cadastrar = () => {
        const {nome, email, senha, senhaRepeticao } = this.state        
        const usuario = {nome,  email, senha, senhaRepeticao }

        try{
            this.service.validar(usuario);
        }catch(erro){
            const msgs = erro.mensagens;
            msgs.forEach(msg => mensagemError(msg));
            return false;
        }

        this.service.salvar(usuario)
            .then( response => {
                mensagemSucesso('Usuário cadastrado com sucesso! Faça o login para acessar o sistema.')
                this.props.history.push('/login')
            }).catch(error => {
                mensagemError(error.response.data)
            })
    }

    render(){
        return (
            <Card title="Cadastro de Usuário">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="bs-component">
                            <FormGroup label="Nome: *" htmlFor="inputNome">
                                <input type="text" 
                                       id="inputNome" 
                                       className="form-control"
                                       name="nome"
                                       onChange={e => this.setState({nome: e.target.value})} />
                            </FormGroup>
                            <FormGroup label="Email: *" htmlFor="inputEmail">
                                <input type="email" 
                                       id="inputEmail"
                                       className="form-control"
                                       name="email"
                                       onChange={e => this.setState({email: e.target.value})} />
                            </FormGroup>
                            <FormGroup label="Senha: *" htmlFor="inputSenha">
                                <input type="password" 
                                       id="inputSenha"
                                       className="form-control"
                                       name="senha"
                                       onChange={e => this.setState({senha: e.target.value})} />
                            </FormGroup>
                            <FormGroup label="Repita a Senha: *" htmlFor="inputRepitaSenha">
                                <input type="password" 
                                       id="inputRepitaSenha"
                                       className="form-control"
                                       name="senha"
                                       onChange={e => this.setState({senhaRepeticao: e.target.value})} />
                            </FormGroup>
                            <button onClick={this.cadastrar} type="button" className="btn btn-success">
                                <i className="pi pi-save"></i> Salvar
                            </button>
                            
                        </div>
                    </div>
                </div>
            </Card>
        )
    }
}

export default withRouter(UsuarioCadastro)